package com.example.homework3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils.isEmpty
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import java.lang.Exception

class MainActivity : AppCompatActivity() {
    private var auth: FirebaseAuth = Firebase.auth

    private lateinit var welcomeButton: LinearLayout
    private lateinit var createButton: Button
    private lateinit var forgotButton: Button
    private lateinit var emailEditText: EditText
    private lateinit var passwordEditText: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val currentUser = auth.currentUser

        welcomeButton = findViewById(R.id.welcomeLinearLayout)
        createButton = findViewById(R.id.createButton)
        forgotButton = findViewById(R.id.forgotButton)
        emailEditText = findViewById(R.id.emailTextField)
        passwordEditText = findViewById(R.id.passwordTextField)

        if(currentUser != null){
            Toast.makeText(this, "Welcome back ${currentUser.email}!", Toast.LENGTH_SHORT).show()
        }
    }

    fun clearInput(view: View){
        emailEditText.setText("")
        passwordEditText.setText("")
    }

    fun signIn(view: View){
        if(isEmpty(emailEditText.text) || isEmpty(passwordEditText.text)){
            Toast.makeText(this, "Please enter credentials",Toast.LENGTH_SHORT).show()
            return
        }

        auth.signInWithEmailAndPassword(emailEditText.text.toString(), passwordEditText.text.toString())
            .addOnCompleteListener(this){
                task -> if(task.isSuccessful){
                    Toast.makeText(this, "Successfully logged in!", Toast.LENGTH_LONG).show()
                } else{
                    Toast.makeText(this, "Could not log in.", Toast.LENGTH_LONG).show()
                }
            }
    }

    fun signUp(view: View){
        if(isEmpty(emailEditText.text) || isEmpty(passwordEditText.text)){
            Toast.makeText(this, "Please enter credentials",Toast.LENGTH_SHORT).show()
            return
        }

        auth.createUserWithEmailAndPassword(emailEditText.text.toString(), passwordEditText.text.toString())
            .addOnCompleteListener(this){
                    task -> if(task.isSuccessful){
                                Toast.makeText(this, "Account Created!", Toast.LENGTH_LONG).show()
                            }
            }
            .addOnFailureListener(this){
                task -> Toast.makeText(this, "Could not Sign Up.", Toast.LENGTH_LONG).show()
                throw Exception(task.stackTraceToString())
            }
    }

    fun unfinished(view: View){
        Toast.makeText(this, "Yikes", Toast.LENGTH_SHORT).show()
    }
}